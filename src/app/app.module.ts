import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { createCustomElement } from '@angular/elements';

import { AppComponent } from './app.component';
import { StoreModule, ActionReducer, MetaReducer } from '@ngrx/store';
import { CustomerReducer } from './customer.reducer';
import { CustomerAddComponent } from './customer-add/customer-add.component';
import { CustomersViewComponent } from './customers-view/customers-view.component';

export function debug(reducer: ActionReducer<any>): ActionReducer<any> {
  return function(state, action) {
    console.log('state', state);
    console.log('action', action);
    return reducer(state, action);
  };
}

export const metaReducers: MetaReducer<any>[] =[debug];

@NgModule({
  declarations: [AppComponent, CustomersViewComponent, CustomerAddComponent],
  imports: [BrowserModule,
    StoreModule.forRoot({ customers: CustomerReducer }, { metaReducers })],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(injector: Injector){
    const productView = createCustomElement(AppComponent, {injector});
    customElements.define('customer-list', productView);
  }
  ngDoBoostrap() {
  }
}
